#include <stdio.h>
#include <memory.h>

char s1[100];
char s2[100];

void concat() {
    strcat(s1, s2);
}

void test() {
    printf("enter the first string: %s\n", s1);
    printf("enter the second string: %s", s2);
    concat(s1, s2);
    printf("the concatenated string:%s ", s1);

}

int main() {
    printf("nhap chuoi s1:");
    fgets(s1, 100, stdin);
    s1[strlen(s1) - 1] = ' ';

    printf("nhap chuoi s2:");
    fgets(s2, 100, stdin);
    test();
    return 0;
}